﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmtpSample
{
    class Program
    {
        static void Main(string[] args)
        {
            MailBeeHelper mailBeeHelper = new MailBeeHelper("smtp.gmail.com","username", "password");
            mailBeeHelper.Connect();
            var mailMessgae = mailBeeHelper.GenerateMessage("user@gmail.com", "Me", "user@gmail.com", "Subject",
                "plain text", "<h1>HTML</h1>");
            mailBeeHelper.Send(mailMessgae);
            mailBeeHelper.Dispose();
        }
    }
}
