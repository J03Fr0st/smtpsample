﻿using MailBee.SmtpMail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailBee;
using MailBee.Mime;

namespace SmtpSample
{
    
    public class MailBeeHelper : IDisposable
    {
        private Smtp _mailer = null;
        private bool _isConnected = false;

        public MailBeeHelper (string name, string accountName, string password) {
            //MailBee.Global.LicenseKey = "MBC900-2535857525-74C75E6FD0CA405C8D8BCF77F01D95E5";
            MailBee.Global.LicenseKey = "MN110-A26A6A676A1C6AF36A065836706E-B343";
            _mailer = new Smtp();
            ConfigureLogs();

            // Disable SSL auto-detection as we need full manual control.
            MailBee.Global.AutodetectPortAndSslMode = true;

            var server = new SmtpServer(name, accountName, password);
            server.AuthMethods = AuthenticationMethods.SaslLogin |
                                 AuthenticationMethods.SaslPlain | AuthenticationMethods.SaslNtlm;
            server.AuthOptions = AuthenticationOptions.PreferSimpleMethods;
            _mailer.SmtpServers.Add(server);
        }

        private void ConfigureLogs() {
            _mailer.Log.Enabled = true;
            _mailer.Log.Filename = @"C:\Maillog.log";
            _mailer.Log.Clear();
        }

        public bool Connect() {
            try {
                _mailer.Connect();
                _mailer.Hello();
                _mailer.Login();
                _isConnected = true;
                return true;
            }
            catch (Exception e) {
                Console.WriteLine(e);
                _isConnected = false;
                return false;
            }
        }

        public void Send(MailMessage mailMessage) {
            if (_isConnected) {
                _mailer.Message = mailMessage;
                var res = _mailer.Send();
            }
            else {
                Console.WriteLine($"Mailer is not connected");
            }

        }

        public MailMessage GenerateMessage(string from,string displayName, string to, string subject, string bodyPlainText,string bodyHtmlText) {
            var mailMessage = new MailMessage {
                From = {
                    Email = @from,
                    DisplayName = displayName
                },
                To = {AsString = to },
                Subject = subject,
                BodyPlainText = bodyPlainText,
                BodyHtmlText = bodyHtmlText,
                Sensitivity = MailSensitivity.Normal,
                Importance = MailPriority.Normal
            };
            return mailMessage;
        }

        public void Dispose() {
            if (_mailer.IsConnected) {
                _mailer?.DisconnectAsync();
            }
            _mailer?.Dispose();
        }
    }
}
